# Quality department's testcase management system

Use the following labels to apply each attribute to the test.

## Test type
The type of automated test.

* ~"type::api": - An API test
* ~"type::browser": - A Browser UI test
* ~"type::visual": - A visual comparison test

## Test suite
* ~"suite::smoke" - Tests that verify fundamental functionality which completes in a short time. Should be executed first.
* ~"suite::reliable" - Tests outside of smoke tests that are deemed reliable.

## Test result

These labels are dynamic and will be used for reporting the status of the test.

* ~"staging::failed" - test is failing on staging environment
* ~"staging::passed" - test is passing on staging environment
* ~"production::failed" - test is failing on production environment
* ~"production::passed" - test is passing on production environment

## Test status
* ~"status::planned" - Test is planned but not yet automated.
* ~"status::automated" - Test has been automated and will receive reporting data from CI.

## Group coverage ownership

Use the following labels to identify which test belongs to which team or stage groups.

* ~Create
* ~Plan
* ~Manage
* ~Verify
* ~Configure
* ~Package
* ~Release
* ~Monitor
* ~Geo
* ~Secure
* etc.
